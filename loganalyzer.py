"""Log Analyzer
   To analyze local or remote data log files from ORION
"""

import pathlib
import sys
from os import path
import logging
from threading import Thread

from PyQt5.QtGui import QIcon, QPalette, QColor
from PyQt5.QtWidgets import (QMainWindow, QAction, QDialog, QFileDialog,
                             QApplication, QPlainTextEdit, QTreeWidget,
                             QTreeWidgetItem, QPushButton, QLineEdit, QHBoxLayout,
                             QVBoxLayout, QDialogButtonBox, QGridLayout,
                             QLabel, QMessageBox, QDockWidget)
from PyQt5.QtCore import pyqtSignal, Qt, QTimer, QObject

from logimporter import LogImporter
from plotcontainer import PlotContainer, SelectionList
from logremoteimporter import RemoteLogImporter

logging.basicConfig(level=logging.DEBUG)
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("urllib3").setLevel(logging.WARNING)


def resource_path(relative_path):
    """
    Get absolute path to resource, works for dev and for PyInstaller $
    """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        # pylint: disable=W0212
        base_path = sys._MEIPASS
    except AttributeError:
        base_path = path.abspath(".")

    return path.join(base_path, "resource", relative_path)


class LoggerHandler(QObject, logging.Handler):
    """
    Gets the logging message and shows in a text field
    """

    addLogSignal = pyqtSignal(str)

    def __init__(self, layout):
        super().__init__()
        self.log_widget = QPlainTextEdit()
        self.log_widget.setMinimumWidth(400)
        self.log_widget.setReadOnly(True)
        layout.addWidget(self.log_widget)
        self.addLogSignal.connect(self.add_log)
        logging.getLogger().addHandler(self)

    def close(self):
        logging.getLogger().removeHandler(self)

    def add_log(self, msg):
        """ Add a log line to text field

        Arguments:
            msg {string} -- Log message
        """

        self.log_widget.textCursor().insertText(msg + "\n")
        self.log_widget.ensureCursorVisible()

    def emit(self, record):
        msg = self.format(record)
        self.addLogSignal.emit(msg)


def get_folder(parent):
    """ Opens a folder selection dialog

    Arguments:
        parent {widget} -- [parent widget]

    Returns:
        str -- chosen folder as absolute path
    """

    dialog = QFileDialog()
    dialog.setFileMode(QFileDialog.Directory)
    dialog.setOption(QFileDialog.ShowDirsOnly)
    return dialog.getExistingDirectory(parent, 'Open Log Folder', '~')


class ImportRemoteDialog(QDialog):
    """ Fetchs remotly the data log from ORION
    """

    logDataDownloaded = pyqtSignal(str)

    def __init__(self, doneSignal):
        super().__init__()

        doneSignal.connect(self.import_done)
        self.setWindowTitle("Import Data Log from ORION")
        self.setWindowModality(Qt.ApplicationModal)
        layout = QVBoxLayout()

        self.ipaddr = QLineEdit()

        self.remote_importer = []
        user_label = QLabel("Username:")
        password_label = QLabel("Password:")
        ip_address_label = QLabel("Hostname:")
        folder_label = QLabel("Download Folder:")
        self.user = QLineEdit("Admin")
        self.password = QLineEdit("orion")
        self.password.setEchoMode(QLineEdit.Password)
        self.downloadfolder = QLineEdit(
            path.join(pathlib.Path.home(), "Downloads"))
        self.downloadfolder.setReadOnly(True)

        self.choose_download_folder_button = QPushButton('Choose')
        self.choose_download_folder_button.setFixedWidth(100)

        remote_grid_layout = QGridLayout()
        remote_grid_layout.addWidget(folder_label, 0, 0)
        remote_grid_layout.addWidget(self.downloadfolder, 0, 1)
        remote_grid_layout.addWidget(self.choose_download_folder_button, 0, 2)
        remote_grid_layout.addWidget(user_label, 1, 0)
        remote_grid_layout.addWidget(self.user, 1, 1)
        remote_grid_layout.addWidget(password_label, 2, 0)
        remote_grid_layout.addWidget(self.password, 2, 1)
        remote_grid_layout.addWidget(ip_address_label, 3, 0)
        remote_grid_layout.addWidget(self.ipaddr, 3, 1)

        # OK and Cancel buttons
        self.buttons = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel,
            Qt.Horizontal, self)
        self.buttons.button(QDialogButtonBox.Ok).setEnabled(False)

        layout.addLayout(remote_grid_layout)
        self.log_handler = LoggerHandler(layout)
        layout.addWidget(self.buttons)

        self.choose_download_folder_button.clicked.connect(self.choose_folder)
        self.buttons.accepted.connect(self.accept)
        self.buttons.rejected.connect(self.reject)
        self.setLayout(layout)

        self.user.textChanged.connect(self.text_changed)
        self.password.textChanged.connect(self.text_changed)
        self.ipaddr.textChanged.connect(self.text_changed)

        self.timer = QTimer()
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.check_change)

    def choose_folder(self):
        """ Used to let the user change the folder
        """
        folder = get_folder(self)
        if folder:
            self.text_changed()
            self.downloadfolder.setText(folder)

    def text_changed(self):
        """Let the system checks the input after a timeout
        """
        self.timer.start(1000)

    def check_change(self):
        """ Proves the input fields, and starts the fetch process
        """
        if self.user.text() and self.password.text() and self.ipaddr.text():
            download_folder = path.join(
                self.downloadfolder.text(), self.ipaddr.text())
            warn_msg = "Overwrite content in folder: {}".format(download_folder)
            if (path.exists(download_folder) and
                    QMessageBox.question(self, 'Message', warn_msg, QMessageBox.Yes, QMessageBox.No) != QMessageBox.Yes):
                return

            importer = RemoteLogImporter(
                download_folder, self.user.text(), self.password.text(), self.ipaddr.text())
            importer.log_data_availabel.connect(self.log_data_availabel)
            importer.start()
            self.remote_importer.append(importer)

    def import_done(self):
        """Import is finalized
        """
        self.buttons.button(QDialogButtonBox.Ok).setEnabled(True)

    def log_data_availabel(self, folder):
        """Notified in case there are some downloaded files available

        Arguments:
            folder {str} -- local folder path
        """

        self.logDataDownloaded.emit(folder)

    def accept(self):
        """Accpet the downloaded files
        """
        self.log_handler.close()
        super().accept()

    def reject(self):
        """Cancel the files
        """
        self.log_handler.close()
        super().reject()


class ImportLocalDialog(QDialog):
    """Local File dialog handler
    """

    logDirChanged = pyqtSignal(str)

    def __init__(self, doneSignal):
        super().__init__()

        doneSignal.connect(self.import_done)

        self.setWindowTitle("Import Data Log from local folder")
        self.setWindowModality(Qt.ApplicationModal)
        layout = QVBoxLayout()

        folder_group_layout = QHBoxLayout()

        self.folder = QLineEdit()
        self.folder.setReadOnly(True)
        palette = QPalette()
        palette.setColor(QPalette.Base, QColor(0xEEEEEE))
        palette.setColor(QPalette.Text, Qt.black)
        self.folder.setPalette(palette)

        folder_label = QLabel("Folder:")
        self.choose_folder_button = QPushButton('Choose')
        self.choose_folder_button.setFixedWidth(100)
        folder_group_layout.addWidget(folder_label)
        folder_group_layout.addWidget(self.folder)
        folder_group_layout.addWidget(self.choose_folder_button)

        # OK and Cancel buttons
        self.buttons = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel,
            Qt.Horizontal, self)
        self.buttons.button(QDialogButtonBox.Ok).setEnabled(False)

        layout.addLayout(folder_group_layout)
        self.log_handler = LoggerHandler(layout)
        layout.addWidget(self.buttons)

        self.choose_folder_button.clicked.connect(self.choose_folder)

        self.buttons.accepted.connect(self.accept)
        self.buttons.rejected.connect(self.reject)
        self.setLayout(layout)

    def choose_folder(self):
        """Open the folder chooser to select local folder
        """

        folder = get_folder(self)
        self.folder.setText(folder)
        if folder:
            self.logDirChanged.emit(folder)

    def accept(self):
        """ Accept the imported files
        """

        self.log_handler.close()
        super().accept()

    def reject(self):
        """ Cancel the imported files
        """

        self.log_handler.close()
        super().reject()

    def import_done(self):
        """ Notify to mark the import is done
        """

        self.buttons.button(QDialogButtonBox.Ok).setEnabled(True)


class MainWindow(QMainWindow):
    """ Main window
    """

    plotAdded = pyqtSignal()

    def __init__(self):
        super().__init__()

        tree = QTreeWidget()
        tree.setSelectionMode(tree.ExtendedSelection)
        tree.setDragDropMode(tree.DragOnly)
        tree.setMinimumWidth(300)
        tree.setHeaderHidden(True)

        self.list_log_config_widget = tree

        self.plot_container = PlotContainer()

        self.setCentralWidget(self.plot_container.widget)
        self.show_data_log_dock()
        self.statusBar()

        self.list_log_config_widget.itemSelectionChanged.connect(
            self.selection_changed)
        self.plot_container.add_plot()

        toolbar = self.addToolBar("File")

        open_action = QAction(
            QIcon(resource_path("open.png")), "open local folder", self)
        open_action.triggered.connect(self.show_import_dolder_dialog)
        toolbar.addAction(open_action)
        download = QAction(QIcon(resource_path("download.png")),
                           "import from ORION", self)
        download.triggered.connect(self.show_import_remote_dialog)
        toolbar.addAction(download)
        new = QAction(QIcon(resource_path("add-graph.png")), "new plot", self)
        new.triggered.connect(self.plot_container.add_plot)
        toolbar.addAction(new)

        show_data_log_action = QAction("&Show Data Log", self)
        show_data_log_action.setStatusTip('View Data')
        show_data_log_action.triggered.connect(self.show_data_log_dock)

        self.statusBar()

        main_menu = self.menuBar()
        file_menu = main_menu.addMenu('&View')
        file_menu.addAction(show_data_log_action)

        #self.setGeometry(300, 300, 800, 600)
        self.setWindowTitle('Log Analyzer')
        self.show()

    def show_data_log_dock(self):
        """Show the dockable widget for log configurations
        """

        tree_dock = QDockWidget("Data Log", self)
        tree_dock.setWidget(self.list_log_config_widget)
        tree_dock.setFloating(False)
        self.addDockWidget(Qt.LeftDockWidgetArea, tree_dock)

    def new_log_config(self, conf, col, events, data_frame, unit):
        """Called by importer to register new log configuration

        Arguments:
            conf {str} -- log configuration name
            col {str} -- the coloumn name
            events {list} -- list of events in case it is an event log
            df {Series} -- panda Series with data
            unit {str} -- unit string
        """

        self.plot_container.update_log_conf_data(conf, col, data_frame, unit)

        list_it = self.list_log_config_widget.findItems(
            conf, Qt.MatchExactly)
        item_it = None
        if not list_it:
            item_it = QTreeWidgetItem([conf])
            item_it.setFlags(Qt.ItemIsEnabled)
            self.list_log_config_widget.addTopLevelItem(item_it)
        else:
            item_it = list_it[0]
        col_it = QTreeWidgetItem([col])
        if events:
            col_it.setFlags(Qt.ItemIsEnabled)
        elif data_frame.dtype == bool:
            col_it.setIcon(0, QIcon(resource_path("up.png")))
        else:
            col_it.setIcon(0, QIcon(resource_path("speed.png")))
        item_it.addChild(col_it)
        for event in sorted(events):
            ev_item = QTreeWidgetItem([event])
            ev_item.setIcon(0, QIcon(resource_path("up.png")))
            col_it.addChild(ev_item)

    def selection_changed(self):
        """Remembers the selected configuration
        """

        items = self.list_log_config_widget.selectedItems()
        tmplist = SelectionList()
        for i in items:
            parent = i.parent()
            grand_parent = parent.parent()
            if not grand_parent:
                # no eventdata
                tmplist.toggle_selection(parent.text(0), i.text(0))
            else:
                # eventlist
                tmplist.toggle_selection(grand_parent.text(0), parent.text(0), i.text(0))
        self.plot_container.selected_item = tmplist

    # pylint: disable=C0103
    def keyPressEvent(self, event):
        """overwrite key press event to delete curves

        Arguments:
            event {event} -- Key pressed
        """

        if event.key() == Qt.Key_Delete:
            self.plot_container.del_selected_curves()
        event.accept()

    def show_import_dolder_dialog(self):
        """Shows the dialog to import form local folder
        """

        log = LogImporter()
        dialog = ImportLocalDialog(log.import_finalized)
        thread = Thread(name='Importer', target=log.do_import)

        def startImport(folder):
            log.set_folder(folder)
            thread.start()
        dialog.logDirChanged.connect(startImport)
        if QDialog.Accepted == dialog.exec():
            thread.join()
            for p in log.toimport:
                self.new_log_config(*p)

    def show_import_remote_dialog(self):
        """Opens the dialog to import form remote ORION
        """

        log = LogImporter()
        dialog = ImportRemoteDialog(log.import_finalized)

        def startImport(folder):
            thread = Thread(name='Importer', target=log.do_import)
            log.set_folder(folder)
            thread.start()
        dialog.logDataDownloaded.connect(startImport)
        if QDialog.Accepted == dialog.exec():
            for p in log.toimport:
                self.new_log_config(*p)


if __name__ == '__main__':
    # pylint: disable=C0103
    application = QApplication(sys.argv)
    application.setWindowIcon(QIcon(resource_path('line-chart.png')))
    main_window = MainWindow()
    sys.exit(application.exec_())
