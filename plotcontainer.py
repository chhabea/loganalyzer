"""Holds all plots with different axis
"""

from functools import partial
from enum import Enum

import pyqtgraph as pg
from pyqtgraph.graphicsItems.LegendItem import LegendItem, ItemSample
from pyqtgraph import AxisItem

import numpy as np
import pandas as pd
from dataclasses import dataclass
from pg_timeaxis import DateAxisItem

__all__ = ["PlotContainer"]

STEP_DATA_UNIT = "StepDataEvent"
AXIS_WIDTH = 70
COLOR_LIST = ["#5555FF",
              "#FF5555",
              "#33AA33",
              "#800000",
              "#2F4F4F",
              "#000080",
              "#8B008B",
              "#FF6347",
              "#C71585",
              "#CD853F",
              "#556B2F",
              "#708090"]

def pick_color(pos):
    """Choos a color according position

    Arguments:
        pos {int} -- number of plot

    Returns:
        color -- the chosen color
    """
    return COLOR_LIST[pos % len(COLOR_LIST)]

def set_index_to_unix_ts(sel):
    """ Converts the index of a data frame to seconds

    Arguments:
        sel {data frame} -- the data frame to change

    Returns:
        data frame -- the changed data frame
    """
    return sel.index.astype(np.int64).values / 1000000000


def create_step_data(name, events, data_frame, curve_item):
    """Create step data out of categorical data

    Arguments:
        name {str} -- The data name, used in legend
        events {list} -- List of distinct events
        data_frame {panda} -- data frame to operate
        curve_item {str} -- curve item]

    Returns:
        [StepData] -- The converted stepdata to plot
    """

    xvalues = data_frame.index.values
    if events:
        event_data = data_frame[data_frame.isin(events)].astype("category")
        index = event_data.cat.categories.values
        series = event_data.cat.codes
        xvalues = event_data.index.values
        if len(events) > 1:
            xvalues = np.append(event_data.index.values, event_data.index.values[-1])
    elif data_frame.dtype == bool:
        index = ["False", "True"]
        series = data_frame.astype(int)
        xvalues = np.append(xvalues, xvalues[-1])

    if name.split("/")[-1] == "Events":
        name = "<br/>".join([e[:20] for e in events])
    return StepData(name, index, xvalues, series, curve_item)

class SelectionList:
    """Handle the selected curve to operate
    """
    def __init__(self):
        self.selection = {}
    def toggle_selection(self, conf, col, event=None):
        """Change the selection status of given curve

        Arguments:
            conf {str} -- log configuraton
            col {str} -- coloumn

        Keyword Arguments:
            event {[type]} -- [description] (default: {None})
        """

        name = "{}/{}".format(conf, col)
        if name not in self.selection:
            self.selection[name] = ((conf, col), [])
        if event:
            self.selection[name][1].append(event)


class Legend(LegendItem):
    """Legend element
    """

    def __init__(self, size=None, offset=None):
        LegendItem.__init__(self, size, offset)

    def paint(self, p, *args):
        p.setPen(pg.mkPen(255, 255, 255, 100, width=2))
        p.setBrush(pg.mkBrush(250, 250, 250, 50))
        p.drawRect(self.boundingRect())

class LegendSample(ItemSample):
    """Data sample in the legend
    """
    def __init__(self, item):
        pg.graphicsItems.LegendItem.ItemSample.__init__(self, item)
        self.setFixedWidth(15)


class PlotType(Enum):
    """ Plot type enumeration"""
    LINE = 1
    STEP = 2
    POINT = 3

def get_plot_type(lst, series):
    """ gets the type of plot

    Arguments:
        lst {list} -- event list
        series {data series} -- data

    Returns:
        PlotType -- the type of plot
    """

    if len(lst) > 1 or series.dtype == bool:
        return PlotType.STEP
    elif len(lst) == 1:
        return PlotType.POINT
    else:
        return PlotType.LINE

class RotateAxis(AxisItem):
    """Axis with the ability to rotate the label
    """

    def __init__(self, *args, **kwargs):
        self.angle = 0
        super(RotateAxis, self).__init__(*args, **kwargs)

    def rotate_tick(self, angle):
        """Rotates the label

        Arguments:
            angle {int} -- angel to rotate
        """

        self.angle = angle

    def draw_picture(self, pen, axis_spec, tick_specs, text_specs):
        """Draw the picture

        Arguments:
            p {pen} -- the pen to draw
            axis_spec {[type]} -- axis detail
            tick_specs {[type]} -- tick details
            text_specs {[type]} -- text details
        """

        pen.setRenderHint(pen.Antialiasing, False)
        pen.setRenderHint(pen.TextAntialiasing, True)

        ## draw long line along axis
        pen, point_1, point_2 = axis_spec
        pen.setPen(pen)
        pen.drawLine(point_1, point_2)
        pen.translate(0.5, 0)

        ## draw ticks
        for temp_pen, point_1, point_2 in tick_specs:
            temp_pen.setPen(pen)
            temp_pen.drawLine(point_1, point_2)

        ## Draw all text
        if self.tickFont is not None:
            pen.setFont(self.tickFont)
        pen.setPen(self.pen())
        for rect, flags, text in text_specs:
            if self.angle == 0:
                pen.drawText(rect, flags, text)
            elif self.angle > 0:
                pen.save()  # save the painter state
                pen.translate(rect.bottomLeft())   # move coordinate system to left of text rect
                pen.rotate(self.angle)  # rotate text
                pen.translate(-rect.bottomLeft())  # revert coordinate system
                pen.drawText(rect, flags, text)
                pen.restore()  # restore the painter state
            else:
                pen.save()  # save the painter state
                pen.translate(rect.bottomRight())   # move coordinate system to left of text rect
                pen.rotate(self.angle)  # rotate text
                pen.translate(-rect.bottomRight())  # revert coordinate system
                pen.drawText(rect, flags, text)
                pen.restore()  # restore the painter state


@dataclass
class AxisViewUnit:
    """Axis data holder
    """
    view: pg.ViewBox
    axis: pg.AxisItem
    unit: str = None

@dataclass
class StepData:
    """Step data holder
    """

    name: str
    index: list
    x: list
    y: pd.Series
    c: None

@dataclass
class Curve:
    """Curve data holder
    """

    name: str
    unit: str
    eventSel: list
    curve: None


class PlotFrame:
    """Consits of one x axis and several y axis
    """

    def __init__(self, p, a):
        self.plot = p
        self.curves = []
        self.axis_view_unit = [a]
        self.step_data = []
        self.selected_curve = []
        self.used_color = []

    def update_step_data(self, index):
        """update the step data of the curve in respect of other stepdata
           all step data share the same yaxis

        Arguments:
            index {index list} -- the possible event values

        Returns:
            index list -- the index list with correct offset to other
        """

        step_data = self.step_data[index]
        yoffset = sum([len(d.index) for d in self.step_data[0:index]])
        totindex = [v for d in self.step_data[0:index+1] for v in d.index]
        totyvalues = step_data.y + yoffset
        return totindex, totyvalues.tolist(), step_data.x

    def remove_step_data(self, name):
        """Remove the stepdata from plot

        Arguments:
            name {str} -- the data name
        """

        found = None
        to_update = []
        for i, elem in enumerate(self.step_data):
            if elem.name == name:
                found = elem
            elif found:
                to_update.append((i-1, elem))
        if found:
            index = []
            self.step_data.remove(found)
            for i, elem in to_update:
                (index, yvalues, xvalues) = self.update_step_data(i)
                elem.c.setData(x=xvalues, y=yvalues)
                self.add_y_axis(index=index)

    def add_curve(self, sel, units, name, events_sel):
        """Add a curve to plot

        Arguments:
            sel {data frame} -- the data
            units {str} -- unit string of data
            name {str} -- the name of the curve
            events_sel {event list} -- selected events in case of step data
        """

        color = self.pick_color()
        curve_item = None
        legendname = name
        plot_type = get_plot_type(events_sel, sel)
        index = []
        if plot_type == PlotType.POINT:
            curve_item = pg.ScatterPlotItem(pen=pg.mkPen(color=color), symbol="o", brush=pg.mkBrush(color), symbolSize=20)
        else:
            curve_item = pg.PlotCurveItem(pen=pg.mkPen(color=color))
            curve_item.setClickable(True, 20)
        if plot_type == PlotType.POINT or plot_type == PlotType.STEP:
            step_data = create_step_data(name, events_sel, sel, curve_item)
            legendname = step_data.name
            self.step_data.append(step_data)
            (index, _, _) = self.update_step_data(len(self.step_data)-1)

        (plot_item, view) = self.add_y_axis(units, index)

        # autorange once to fit views at start
        if self.curves:
            view.enableAutoRange(axis=pg.ViewBox.YAxis, enable=True)
            view.enableAutoRange(axis=pg.ViewBox.XAxis, enable=False)
        else:
            view.enableAutoRange(axis=pg.ViewBox.XYAxes, enable=True)
        self.curves.append(Curve(name, units, events_sel, curve_item))

        #cI=pg.PlotDataItem(xvalues, sel.tolist(), symbol="o", brush=pg.mkBrush(color), symbolSize=4,
        #    pen=pg.mkPen(color=color, width=2), stepMode=stepMode)
        plot_item.legend.addItem(item=LegendSample(curve_item), name=legendname)
        curve_item.sigClicked.connect(partial(self.toggle_selected_curve, color, view, legendname))
        view.addItem(curve_item)
        # updates when resized
        view.sigResized.connect(self.update_views)
        self.update_views(None)

    def update_curve(self, sel, events_sel, curve_item):
        """Update the data of the curve item

        Arguments:
            sel {data series} -- data
            events_sel {event list} -- list of events
            curve_item {curve item} -- curveto update
        """

        xvalues = sel.index.values
        yvalues = sel.tolist()

        plot_type = get_plot_type(events_sel, sel)
        if plot_type == PlotType.POINT or plot_type == PlotType.STEP:
            idx = [i for i, s in enumerate(self.step_data) if s.c == curve_item][0]
            self.step_data[idx] = create_step_data(self.step_data[idx].name, events_sel, sel, curve_item)
            (_, yvalues, xvalues) = self.update_step_data(idx)

        curve_item.setData(xvalues, yvalues, stepMode=plot_type == PlotType.STEP)

    def add_y_axis(self, unit=None, index=None):
        """search for an unused y axis

        Keyword Arguments:
            unit {str} -- the unit of the axis (default: {None})
            index {list} -- index for the step data (default: {[]})

        Returns:
            [type] -- [description]
        """
        if index:
            unit = STEP_DATA_UNIT
        selected_axis = [(avu_idx, avu) for avu_idx, avu in enumerate(self.axis_view_unit) if avu.unit in [None, unit]]

        if selected_axis and  unit:
            avu_idx, avu = selected_axis[0]
            self.axis_view_unit[avu_idx] = AxisViewUnit(avu.view, avu.axis, unit)
            avu.axis = self.configure_y_axis(avu.axis, unit, index)
            return (self.plot, avu.view)

        avu = RotateAxis("right")
        avu = self.configure_y_axis(avu, unit, index)
        view = pg.ViewBox()
        self.append_axis_view_unit(AxisViewUnit(view, avu, unit))

        self.plot.layout.addItem(avu, 2, self.get_number_axis_view_unit())
        self.plot.scene().addItem(view)
        avu.linkToView(view)
        vprev = self.axis_view_unit[0].view
        view.setXLink(vprev)
        return (self.plot, view)

    def configure_y_axis(self, axis, unit, index):
        """prepare the y axis

        Arguments:
            axis {axis} -- the axis
            unit {str} -- the unit of the axis
            index {list} -- the index in case of step data

        Returns:
            axis -- the final axis
        """

        axis.setWidth(AXIS_WIDTH)
        if unit == STEP_DATA_UNIT:
            if axis.orientation == "left":
                axis.rotate_tick(-67.5)
            else:
                axis.rotate_tick(67.5)
            index = [i[0:20] for i in index]
            axis.setTicks([dict(enumerate(index)).items()])
        else:
            axis.setLabel(units=unit, color='#000000')
        return axis

    def update_views(self, _):
        """slot: update view when resized

        Arguments:
            event {event} -- [the qt event
        """

        avu_list = self.axis_view_unit
        first_view = avu_list[0].view
        for avu in avu_list[1:]:
            avu.view.setGeometry(first_view.sceneBoundingRect())

    def set_x_range(self, xrange):
        """Set the x axis xrange

        Arguments:
            xrange {range} -- x limits for the axis
        """
        self.plot.setXRange(*xrange, padding=0)

    def append_axis_view_unit(self, avu):
        """Add axis to the axis list

        Arguments:
            avu {AxisViewUnit} -- Axis container
        """
        self.axis_view_unit.append(avu)

    def get_number_axis_view_unit(self):
        """Return the numbers of axis in the polt

        Returns:
            [int] -- number of y axis
        """
        return len(self.axis_view_unit)

    def pick_color(self):
        """Select a color out of color list

        Returns:
            color -- color to use
        """

        for color in COLOR_LIST:
            if color not in self.used_color:
                self.used_color.append(color)
                return color
        return COLOR_LIST[0]

    def del_selected_curves(self):
        """remove all selected curves
        """

        for view, curve_to_remove, color, name in self.selected_curve:
            view.removeItem(curve_to_remove)
            self.curves.remove([curve for curve in self.curves if curve.curve == curve_to_remove][0])
            if color in self.used_color:
                self.used_color.remove(color)
            self.plot.legend.removeItem(name)
            self.remove_step_data(name)

        self.selected_curve = []

    def toggle_selected_curve(self, color, view, legend, curve_item):
        """Selects/deselect the curve

        Arguments:
            color {color} -- color to restore on deselection
            v {view} -- view to remember
            legend {legend} -- legend item to remeber
            curve_item {curve item} -- curve item to remember
        """

        item = (view, curve_item, color, legend)
        if  item in self.selected_curve:
            curve_item.setPen(pg.mkPen(color=color))
            self.selected_curve.remove(item)
        else:
            curve_item.setPen(pg.mkPen(color="#00FFFF"))
            self.selected_curve.append(item)

class PlotContainer:
    """Hold all plot of window
    """

    axisWidht = 70
    def __init__(self):
        pg.setConfigOptions(antialias=True)
        pg.setConfigOption('background', "#FDFDFD")
        pg.setConfigOption('foreground', '#020202')
        self.widget = pg.GraphicsLayoutWidget()
        self.widget.setStyleSheet("border:1px solid rgb(180, 180, 180); ")
        self.widget.show()
        self.widget.dragEnterEvent = self.drag_enter_event
        self.plots = []
        self.list_log_config_data = {}
        self.selected_item = SelectionList()

    def update_log_conf_data(self, conf, col, data_frame, unit):
        """adds or update log data

        Arguments:
            conf {str} -- the log name
            col {str} -- the log configuration name
            data_frame {data frame} -- the data
            unit {str} -- the unit name
        """

        if conf not in self.list_log_config_data:
            self.list_log_config_data[conf] = {}

        data_frame.index = set_index_to_unix_ts(data_frame)
        self.list_log_config_data[conf][col] = (data_frame, unit, "{}/{}".format(conf, col))
        self.update_plot_data(conf, col)

    def drag_enter_event(self, event):
        """ event if sombody try to drag something

        Arguments:
            ev {evnt} -- qt event
        """
        event.accept()

    # pylint: disable=W0613
    def add_plot(self, event=None):
        """Adds a new plot the windows

        Keyword Arguments:
            event {event} -- qt event  (default: {None})
        """

        if self.plots:
            self.widget.nextRow()

        plot_item = pg.PlotItem(axisItems={"left":RotateAxis("left")})
        view = plot_item.vb # reference to viewbox of the plotitem
        self.widget.addItem(plot_item) # add plotitem to layout

        plot_item.dropEvent = partial(self.add_selection_to_plot, len(self.plots))
        leg = Legend(offset=(30, 30))
        leg.setParentItem(plot_item.getViewBox())
        plot_item.legend = leg
        plot_item.setAcceptDrops(True)
        xaxis = DateAxisItem(orientation='bottom')
        xaxis.attachToPlotItem(plot_item)
        plot_item.showGrid(x=True, y=True)
        yaxis = plot_item.getAxis("left")
        yaxis.setWidth(PlotContainer.axisWidht)

        self.plots.append(PlotFrame(plot_item, AxisViewUnit(view, yaxis)))
        plot_item.sigXRangeChanged.connect(lambda: self.update_x_axis(plot_item.getViewBox().viewRange()[0]))

#       workaround to keep the plot rect identical
        if len(self.plots) > 1:
            for _ in range(len(self.plots[0].axis_view_unit)-1):
                self.plots[-1].add_y_axis()

    # pylint: disable=W0613
    def add_selection_to_plot(self, plot_nbr, event=None):
        """Add a curve to plot

        Arguments:
            plot_nbr {int} -- the plot number

        Keyword Arguments:
            event {event} -- [the qt event] (default: {None})
        """
        plot = self.plots[plot_nbr]
        for _, ((conf, col), event_list) in self.selected_item.selection.items():
            (sel, unit, name) = self.list_log_config_data[conf][col]
            plot.add_curve(sel, unit, name, event_list)
            plot.update_curve(sel, event_list, plot.curves[-1].curve)

            #   workaround to keep the plot size width the same in all plots
            for i, plot_it in enumerate(self.plots):
                if (i != plot_nbr and unit and
                        plot_it.get_number_axis_view_unit() < plot.get_number_axis_view_unit()):
                    plot_it.add_y_axis()

    def update_plot_data(self, conf, col):
        """update the plot data

        Arguments:
            conf {str} -- the log configuration
            col {str} -- the couloumn
        """

        (sel, unit, name) = self.list_log_config_data[conf][col]
        for plot in self.plots:
            for curve in plot.curves:
                if curve.name == name and curve.unit == unit:
                    plot.update_curve(sel, curve.eventSel, curve.curve)

    def update_x_axis(self, region):
        """ set the xrange for all plots

        Arguments:
            region {range} -- x axis limits
        """
        for plot in self.plots:
            plot.set_x_range(region)

    def del_selected_curves(self):
        """ remove selected curves from all plots
        """
        for plot in self.plots:
            plot.del_selected_curves()
