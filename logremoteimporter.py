"""Remote data log importer for ORION
"""

from os import path, makedirs
from threading import Thread
import urllib.parse
import logging

import untangle
import requests
from requests.exceptions import ConnectionError as conerr, HTTPError
from PyQt5.QtCore import pyqtSignal, QObject

__all__ = ["RemoteLogImporter"]


HEADERS = {'content-type': 'application/x-www-form-urlencoded'}

LOGINDATA = '''
<?xml version="1.0" encoding="UTF-8"?>
<d>
  <l un="{}" pw="{}">
</d>
'''
TEST_URL = "http://{}"
LOGIN_URL = "http://{}/rest/1.1/login"
LOGOUT_URL = "http://{}/rest/1.1/logout?sid={}"
FILE_LIST_URL = "http://{}/rest/1.1/file/list?sid={}&type=logfile"
GET_FILE_URL = "http://{}/rest/1.1/file/{}?sid={}&type=logfile"


class RemoteLogImporter(QObject, Thread):
    """Class to import data log from remote ORION
    """

    log_data_availabel = pyqtSignal(str)

    def __init__(self, d, user, password, ip):
        super().__init__()
        self.ip_address = ip
        self.password = password
        self.user = user
        self.dir = d
        self.session = ""

    def logout(self):
        """Gracefully logut the session
        """

        if self.session:
            requests.post(LOGOUT_URL.format(self.ip_address, self.session))

    def run(self):
        """Connected remotely and fetch all data log
        """

        response = None
        try:
            response = requests.get(TEST_URL.format(self.ip_address), timeout=5)
            response.encoding = 'utf-8'
            if response.status_code != 200:
                response.raise_for_status()
        except conerr as exception:
            logging.warning("Not reachable: %s", self.ip_address)
            return

        try:
            # pylint: disable=C0301
            response = requests.post(LOGIN_URL.format(self.ip_address),
                                     data=urllib.parse.urlencode({"data": LOGINDATA.format(self.user, self.password)}),
                                     headers=HEADERS, timeout=5)
            response.encoding = 'utf-8'
            if response.status_code != 200:
                response.raise_for_status()
        except HTTPError as exception:
            logging.warning("Failed to connect %s. %s", self.ip_address, exception)
            return

        try:
            if not path.exists(self.dir):
                makedirs(self.dir)

            obj = untangle.parse(response.text)
            self.session = obj.d.l["s"]

            response = requests.get(FILE_LIST_URL.format(self.ip_address, self.session))
            response.encoding = 'utf-8'
            if response.status_code != 200:
                response.raise_for_status()

            obj = untangle.parse(response.text)
            availabel = False
            for file_name in obj.d.f:
                filename = file_name.cdata
                logging.info("Fetch data log %s.", filename)
                response = requests.get(GET_FILE_URL.format(self.ip_address, filename, self.session), stream=True)
                response.encoding = 'utf-8'
                if response.status_code == 200:
                    with open(path.join(self.dir, filename), "wb") as file:
                        for chunk in response.iter_content(chunk_size=128):
                            file.write(chunk)
                        availabel = True

            if availabel:
                self.log_data_availabel.emit(self.dir)
                logging.info("Download Finished")

            else:
                logging.info("No Data Log found at %s.", self.ip_address)


        except Exception as exception:
            print(type(exception))
            logging.warning("Failed request %s. %s", self.ip_address, exception)

        self.logout()

if __name__ == "__main__":
    IMPORTER = RemoteLogImporter("/home/anton/Downloads", "Admin", "orion", "www.google.com")
    IMPORTER.start()
    IMPORTER.join()
    