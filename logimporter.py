"""Data importer for loganalyzer.
"""

from os import listdir, path
import logging
import re

import pandas as pd
from PyQt5.QtCore import pyqtSignal, QObject

__all__ = ["LogImporter"]

class LogImportError(Exception):
    """Import specifcexcpetion
    """

    def __init__(self, value):
        super().__init__()
        self.value = value
    def __str__(self):
        return repr(self.value)

class LogImporter(QObject):
    """Import the data log from local folder
    """

    import_finalized = pyqtSignal()
    def __init__(self):
        super().__init__()
        self.toimport = []
        self.folder = None

    def set_folder(self, folder):
        """Set the folder to import

        Arguments:
            folder {str} -- path of directorie
        """

        self.folder = folder

    def do_import(self):
        """Starts the import of previously chosen directory
        """

        self.toimport = []
        dfs = {}
        units = {}
        for file in listdir(self.folder):
            name = file.replace(" ", "_")
            log_type = "_".join(name.split("_")[1:-1])
            if log_type not in dfs:
                dfs[log_type] = []
                units[log_type] = {}
            try:
                # compile regex
                skip_line = None
                sep = None
                regex = re.compile('Date/Time(.)')
                file_name = path.join(self.folder, file)
                with open(file_name, 'rb') as input_file:
                    for line_i, line in enumerate(input_file):
                        match = regex.search(str(line))
                        if match:
                            skip_line = line_i
                            sep = match.group(1)
                if not sep:
                    raise LogImportError("DateTime not found")
                data_frame, ulist = self.convert_data_frame(pd.read_csv(file_name, na_values=["****", "#VALUE!"],
                                                                        skiprows=skip_line, encoding="utf8", sep=sep))
                logging.info("Read %s, %d lines", file, data_frame.shape[0])

                for col, unit in zip(data_frame.columns, ulist):
                    units[log_type][col] = unit
                if data_frame.shape[0] > 0:
                    dfs[log_type].append(data_frame)
            except (LogImportError, TypeError, AttributeError, ValueError) as exception:
                logging.warning("Ignored file: %s (%s)", file, exception)

        for log_type, dfstype in dfs.items():
            if dfstype:
                dfcon = pd.concat(dfstype, axis=0, sort=False)
                if dfcon.shape[0] == 0:
                    continue
                dfcon.sort_values("DateTime", inplace=True)
                dfcon.set_index("DateTime", inplace=True)
                event_series = pd.Series()
                event_name = "Events"
                if event_name in dfcon.columns:
                    event_series = dfcon.Events
                    event_series = event_series.dropna()
                    dfcon = dfcon[dfcon.Events.isna()]
                    dfcon = dfcon.drop(event_name, axis=1)

                for i, _ in enumerate(dfcon.dtypes):
                    col = dfcon.columns[i]
                    if dfcon.dtypes[i] == float or str(dfcon.dtypes[i]) == "int64":
                        dfcon.iloc[:, i] = pd.to_numeric(dfcon.iloc[:, i])
                        self.toimport.append((log_type, col, [], dfcon[col], units[log_type][col]))
                    elif dfcon.dtypes[i] == object:
                        dfcon[col] = dfcon[col].astype("bool")
                        self.toimport.append((log_type, col, [], dfcon[col], ""))
                if event_series.shape[0] > 0:
                    event_series = event_series.astype("str")
                    self.toimport.append((log_type, event_name, event_series.unique().tolist(),
                                          event_series, ""))
        logging.info("Finished")
        self.import_finalized.emit()

    def convert_data_frame(self, data_frame):
        """Converts a single raw datalogfile with correct data types

        Arguments:
            data_frame {data frame} -- raw panda

        Returns:
            [data frame] -- cleaned data frame with data type
        """

        units = []
        colums = []
        for col in data_frame.columns:
            # pylint: disable=W1401
            match = re.search('(.*) \[(.*)\]$', col)
            if match:
                colums.append(match[1])
                units.append(match[2])
            else:
                colums.append(col)
                units.append("")
        data_frame.columns = [
            # pylint: disable=C0301
            c.replace("[", "").replace("]", "").replace(" ", "_").replace("/", "").replace("°", "").replace("%", "") for
            c in colums]
        data_frame.DateTime = pd.to_datetime(data_frame.DateTime, dayfirst=True)
        data_frame = data_frame[data_frame.DateTime > "2018-01-10"]
        return (data_frame, units)
